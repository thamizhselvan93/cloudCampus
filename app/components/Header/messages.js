/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  register: {
    id: 'boilerplate.components.Header.register',
    defaultMessage: 'Register Student',
  },
  view: {
    id: 'boilerplate.components.Header.view',
    defaultMessage: 'View Record',
  },
});
