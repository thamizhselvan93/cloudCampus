/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  pma: {
    id: 'boilerplate.components.Footer.pma',
    defaultMessage: 'This project is done for Pondicherry Maritime Academy',
  },
  authorMessage: {
    id: 'boilerplate.components.Footer.author.message',
    defaultMessage: `
      Made with love by True Softwares
    `,
  },
});
