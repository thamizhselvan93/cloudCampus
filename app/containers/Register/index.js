/**
 *
 * Register
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectRegister from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export class Register extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <Helmet>
          <title>Register</title>
          <meta name="description" content="Description of Register" />
        </Helmet>
        <FormattedMessage {...messages.header} />
        <form  onSubmit={this.handleSubmit}>
        <p>
          <label>
            Name:
            <input type="text" name="name" value={this.state.value} onChange={this.handleChange} />
          </label>
        </p>
        <p>
          <label>
            Age:
            <input type="text" name="name" value={this.state.value} onChange={this.handleChange} />
          </label>
        </p>
        <p>
          <label>
            Gender:
            <input type="text" name="name" value={this.state.value} onChange={this.handleChange} />
          </label>
        </p>
        <p>
          <label>
            Location:
            <input type="text" name="name" value={this.state.value} onChange={this.handleChange} />
          </label>
        </p>
        <p>
          <label>
            Qualification:
            <input type="text" name="name" value={this.state.value} onChange={this.handleChange} />
          </label>
        </p>
        <div>
          <input type="submit" value="Submit" />
        </div>
        </form>
      </div>
    );
  }
}

Register.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  register: makeSelectRegister(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'register', reducer });
const withSaga = injectSaga({ key: 'register', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Register);
